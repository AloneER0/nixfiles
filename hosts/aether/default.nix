{pkgs, ...}: {
  imports = [
    ../../nixosModules
  ];

  custom = {
    # Software
    gaming.enable = true;

    # DE
    plasma.enable = true;
    #gnome.enable = true;
    #cinnamon.enable = true;

    # Themes
    themeing.enable = true;

    # Fonts
    allfonts.enable = true;

    # Drivers
    nvidia.enable = true;
  };

  services.xserver = {
    enable = true;
    xkb = {
      layout = "pl";
      variant = "";
    };

    updateDbusEnvironment = true;

    excludePackages = [pkgs.xterm];
  };
}
