{
  lib,
  stdenv,
  fetchFromGitHub,
  libjxl,
  imagemagick,
  makeWrapper,
  kdePackages,
  exiftool,
  optipng,
  pngquant,
}:
stdenv.mkDerivation rec {
  pname = "kf6-servicemenus-imagetools";
  version = "3";

  src = fetchFromGitHub {
    owner = "marco-mania";
    repo = "kf6-servicemenus-imagetools";
    rev = "v${version}";
    hash = "sha256-2XibyThS9VGKsdy5HOnqbEfWTDHTUHwwjfGrJy/vwSM=";
  };

  propagatedBuildInputs = [
    libjxl
    imagemagick
    exiftool
    optipng
    pngquant
    kdePackages.kdialog
    kdePackages.qttools
  ];

  dontWrapQtApps = true;

  buildInputs = [
    makeWrapper
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp bin/* $out/bin/

    mkdir -p $out/share/kio/servicemenus
    cp servicemenus/* $out/share/kio/servicemenus
  '';

  postFixup = ''
    wrapProgram $out/bin/imagetools-kdialog \
      --prefix PATH : ${
      lib.makeBinPath [
        libjxl
        imagemagick
        exiftool
        optipng
        pngquant
        kdePackages.qttools
        kdePackages.kdialog
      ]
    }
  '';

  meta = with lib; {
    description = "KDE service menus for image file processing";
    homepage = "https://github.com/marco-mania/kf6-servicemenus-imagetools";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [Alxhr0];
    mainProgram = "kf6-servicemenus-imagetools";
    platforms = platforms.all;
  };
}
