{
  lib,
  rustPlatform,
  fetchFromGitLab,
}:
rustPlatform.buildRustPackage rec {
  pname = "nix-gen";
  version = "unstable-2024-12-18";

  src = fetchFromGitLab {
    owner = "alxhr0";
    repo = "nix-gen";
    rev = "731625b1ff23aa934ebeb3ef6066e3ecbeddce10";
    hash = "sha256-E2/z2KSgLPjWQ4wMTcTtSITHKMET9wuUxzOXCoeTLhE=";
  };

  cargoHash = "sha256-EMqS1oabNft41ILZ7tMOGl3CdTvYLtTS9IOFplCv6YA=";

  meta = {
    description = "Nix-gen shows current NixOS generation without needing root permissions";
    homepage = "https://gitlab.com/alxhr0/nix-gen";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [];
    mainProgram = "nix-gen";
  };
}
