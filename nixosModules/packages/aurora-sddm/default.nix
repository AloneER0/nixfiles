{
  lib,
  stdenv,
}:
stdenv.mkDerivation {
  pname = "aurora-sddm";
  version = "1.0";

  src = ./.;

  installPhase = ''
    mkdir -p $out
    cp -R ./* $out/
  '';

  meta = {
    description = "Modified Dracula SDDM theme";
    homepage = "";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [Alxhr0];
    mainProgram = "sddm-eucalyptus-drop";
    platforms = lib.platforms.all;
  };
}
