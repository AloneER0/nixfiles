{
  lib,
  stdenv,
  xmlstarlet,
  librsvg,
  xorg,
  fetchFromGitHub,
}:
stdenv.mkDerivation rec {
  pname = "mocu-xcursor";
  version = "1.1";

  src = fetchFromGitHub {
    owner = "sevmeyer";
    repo = "mocu-xcursor";
    rev = version;
    hash = "sha256-DVHPUCq3y/f1cVHHKg/qXYr/pGGUcP98RhFuGzNhT/I=";
  };

  nativeBuildInputs = [
    xmlstarlet
    librsvg
    xorg.xcursorgen
  ];

  buildPhase = ''
    runHook preBuild

    patchShebangs make.sh
    ./make.sh

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -dm 0755 $out/share/icons
    cp -rf dist/* $out/share/icons/

    runHook postInstall
  '';

  meta = {
    description = "A modest xcursor theme";
    homepage = "https://github.com/sevmeyer/mocu-xcursor";
    changelog = "https://github.com/sevmeyer/mocu-xcursor/blob/${src.rev}/CHANGELOG.md";
    license = lib.licenses.cc0;
    maintainers = with lib.maintainers; [];
    mainProgram = "mocu-xcursor";
    platforms = lib.platforms.all;
  };
}
