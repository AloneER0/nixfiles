{
  lib,
  rustPlatform,
  fetchFromGitLab,
}:
rustPlatform.buildRustPackage rec {
  pname = "hourglass";
  version = "unstable-2024-12-18";

  src = fetchFromGitLab {
    owner = "alxhr0";
    repo = "hourglass";
    rev = "62ecc492abf87bce1063821ed0d3f3dca2e27b21";
    hash = "sha256-5PH4ke6iJWetRmIAtXUTyETRxEM9cqFS9gOCWnjN1I8=";
  };

  cargoHash = "sha256-cf8/W+XSlADf6519e7qaa13x3EMDZu/EpnOf2vl/ngA=";

  meta = {
    description = "";
    homepage = "https://gitlab.com/alxhr0/hourglass";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [];
    mainProgram = "hourglass";
  };
}
