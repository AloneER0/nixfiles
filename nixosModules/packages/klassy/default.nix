{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  kdePackages,
  xorg,
  gettext,
}:
stdenv.mkDerivation rec {
  pname = "klassy";
  version = "6.2.breeze6.2.1";

  src = fetchFromGitHub {
    owner = "paulmcauley";
    repo = "klassy";
    rev = version;
    hash = "sha256-tFqze3xN1XECY74Gj0nScis7DVNOZO4wcfeA7mNZT5M=";
  };

  propagatedBuildInputs = [
    kdePackages.extra-cmake-modules
    kdePackages.kcmutils
    kdePackages.kcolorscheme
    kdePackages.kconfig
    kdePackages.kconfigwidgets
    kdePackages.kcoreaddons
    kdePackages.kguiaddons
    kdePackages.ki18n
    kdePackages.kiconthemes
    kdePackages.kirigami
    kdePackages.kpackage
    kdePackages.kservice
    kdePackages.kwindowsystem
    kdePackages.kwayland
    xorg.libX11
    kdePackages.libplasma
    kdePackages.qtbase
    kdePackages.qtsvg
    kdePackages.qtdeclarative
    kdePackages.kdecoration
    gettext
  ];

  cmakeFlags = [
    "-DKDE_INSTALL_USE_QT_SYS_PATHS=ON"
    "-DBUILD_QT5=OFF"
  ];

  nativeBuildInputs = [
    cmake
  ];

  dontWrapQtApps = true;

  meta = {
    description = "Klassy is a highly customizable binary Window Decoration, Application Style and Global Theme plugin for recent versions of the KDE Plasma desktop";
    homepage = "https://github.com/paulmcauley/klassy";
    license = lib.licenses.unfree; # FIXME: nix-init did not find a license
    maintainers = with lib.maintainers; [];
    mainProgram = "klassy";
    platforms = lib.platforms.all;
  };
}
