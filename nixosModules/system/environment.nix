{config, ...}: {
  # Set Environment Variables
  environment = {
    sessionVariables = {
      FLAKE = "/etc/nixos";
      NIXOS_OZONE_WL = "1";
      GSK_RENDERER = "gl";
    };
  };
}
