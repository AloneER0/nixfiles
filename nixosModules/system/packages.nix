{pkgs, ...}: {
  # Enable/Disable programs
  programs = {
    nano.enable = false;
    firefox = {
      enable = true;
      package = pkgs.firefox-devedition-bin;
    };
    thunderbird.enable = true;
    obs-studio.enable = true;
    nh.enable = true;
    htop.enable = true;
    adb.enable = true;
    zsh.enable = true;
  };

  services = {
    flatpak.enable = true;
  };

  environment = {
    systemPackages = with pkgs; [
      # School
      ciscoPacketTracer8

      # IDE
      jetbrains.webstorm

      # Internet
      vesktop
      element-desktop
      nextcloud-client
      bitwarden-desktop
      microsoft-edge

      # Graphics
      gimp
      krita
      #kdePackages.kcolorchooser
      #(callPackage ../packages/kf6-servicemenus-imagetools { })

      # Office
      onlyoffice-bin
      #libreoffice-qt6-fresh
      qbittorrent

      # Themes/Icons
      tela-icon-theme
      papirus-icon-theme
      papirus-folders
      bibata-cursors

      # Media
      vlc
      audacity

      # Utilities
      (callPackage ../packages/hourglass {})
      (callPackage ../packages/nix-gen {})
      (uutils-coreutils.override {prefix = "";})
      appimage-run
      btop
      godot_4
      kdePackages.xwaylandvideobridge
      fastfetch
      distrobox
      cava
      wgetpaste
      nix-init
      ripgrep
      nixfmt-rfc-style
      fd
      pciutils
      aria2
      jq
      pandoc
      pfetch-rs
      unrar
      gparted
      gh
      hyfetch
      git
      lutgen
    ];
  };
}
