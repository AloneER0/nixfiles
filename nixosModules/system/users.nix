{pkgs, ...}: {
  # Setup users on the system
  users = {
    users = {
      alxhr = {
        isNormalUser = true;
        description = "Alxhr0";
        shell = pkgs.zsh;
        extraGroups = ["networkmanager" "wheel" "libvirtd" "vboxusers"];
      };
    };
  };
}
