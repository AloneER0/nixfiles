{
  inputs,
  pkgs,
  ...
}: {
  # Configure Nix
  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };

    settings = {
      system-features = ["big-parallel" "gccarch-x86-64-v3"];
      substituters = ["https://nix-community.cachix.org" "https://cache.nixos.org/"];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      experimental-features = ["nix-command" "flakes"];
    };

    nixPath = ["nixpkgs=${inputs.nixpkgs}"];
  };

  environment.systemPackages = with pkgs; [
    nixd
    alejandra
  ];

  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "24.11";
}
