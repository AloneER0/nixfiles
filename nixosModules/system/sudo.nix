{config, ...}: {
  # Configure Sudo
  security = {
    sudo = {
      wheelNeedsPassword = false;
    };
  };
}
