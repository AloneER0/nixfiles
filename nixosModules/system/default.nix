{
  imports = [
    ./packages.nix
    ./users.nix
    ./sudo.nix
    ./environment.nix
    ./nix.nix
    ./locale.nix
  ];
}
