{
  imports = [
    ./desktop
    ./hardware
    ./gaming
    ./system
    ./themes
    ./windowmanager
  ];
}
