{
  config,
  lib,
  pkgs,
  ...
}: {
  # Configure Plasma
  options.custom = with lib; {
    river.enable = mkEnableOption "River";
  };

  config = lib.mkIf config.custom.river.enable {
    # Enable SDDM
    services.displayManager = {
      sddm = {
        enable = true;
      };
    };

    programs.river = {
      enable = true;
    };
  };
}
