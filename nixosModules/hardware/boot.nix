{pkgs, ...}: {
  # Bootloader
  boot = {
    kernel.sysctl = {
      "vm.swappiness" = 10;
    };

    kernelParams = ["amd_iommu=on" "iommu=pt" "iwlwifi.power_save=0"];

    kernelPackages = pkgs.linuxPackages_xanmod_latest;

    plymouth = {
      enable = true;
    };

    loader = {
      # Allow to change EFI Variables
      efi = {
        canTouchEfiVariables = true;
      };

      # Enable systemd-boot
      systemd-boot = {
        enable = true;
      };
    };
  };


  nixos-boot = {
    enable = true;
  };
}
