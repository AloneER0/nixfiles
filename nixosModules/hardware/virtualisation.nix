{...}: {
  # VM stuff

  programs.virt-manager.enable = true;

  virtualisation = {
    podman.enable = true;
    libvirtd.enable = true;
    virtualbox = {
      host.enable = true;
    };
  };
}
