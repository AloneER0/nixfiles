{
  imports = [
    ./audio.nix
    ./network.nix
    ./nvidia.nix
    ./boot.nix
    ./disks.nix
    ./virtualisation.nix
    ./openrgb.nix
  ];
}
