{
  config,
  pkgs,
  ...
}: {
  # Networking
  networking = {
    # Set Hostname
    hostName = "aether";

    # Enable NetworkManager
    networkmanager = {
      enable = true;
    };
  };
}
