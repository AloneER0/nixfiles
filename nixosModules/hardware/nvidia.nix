{
  config,
  lib,
  ...
}: {
  options.custom = with lib; {
    nvidia.enable = mkEnableOption "Enable NVIDIA GPU";
  };

  config = lib.mkIf config.custom.nvidia.enable {
    services.xserver.videoDrivers = ["nvidia"];

    # NVIDIA drivers
    hardware = {
      nvidia = {
        package = config.boot.kernelPackages.nvidiaPackages.stable;

        open = true;

        modesetting = {
          enable = true;
        };

        prime = {
          sync.enable = true;
          nvidiaBusId = "PCI:1:0:0";
          amdgpuBusId = "PCI:5:0:0";
        };
      };

      graphics = {
        enable32Bit = true;
      };
    };
  };
}
