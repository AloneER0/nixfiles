{pkgs, ...}: {
  # Audio setup

  security.rtkit.enable = true;

  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;

      wireplumber = {
        extraConfig = {
          "monitor.bluez.rules" = {
            actions = {
              uodate-props = {
                "bluetooth.autoswitch-to-headset-profile" = "false";
              };
            };
          };
        };
      };
    };
  };

  hardware = {
    bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Control,Gateway,Headset,Media,Sink,Socket,Source";
        };
      };
    };

    pulseaudio = {
      enable = false;
    };
  };

  environment.systemPackages = with pkgs; [pavucontrol];
}
