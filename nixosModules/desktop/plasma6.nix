{
  config,
  lib,
  pkgs,
  ...
}: let
  sddmTheme = pkgs.callPackage ../packages/aurora-sddm {};
in {
  # Configure Plasma
  options.custom = with lib; {
    plasma.enable = mkEnableOption "Plasma Desktop";
  };

  config = lib.mkIf config.custom.plasma.enable {
    services.desktopManager = {
      plasma6 = {
        enable = true;
      };
    };

    # Enable SDDM
    services.displayManager = {
      sddm = {
        enable = true;
        theme = "${sddmTheme}";
      };
    };

    programs.kde-pim = {
      enable = true;
      merkuro = true;
    };

    environment = {
      systemPackages = with pkgs; [
        # KDE Packages
        kdePackages.kdepim-addons
        kdePackages.yakuake
        (callPackage ../packages/kf6-servicemenus-imagetools {})
        (callPackage ../packages/klassy {})
      ];

      plasma6 = {
        excludePackages = with pkgs; [
          kdePackages.krdp
        ];
      };
    };
  };
}
