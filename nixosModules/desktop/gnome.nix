{
  config,
  pkgs,
  lib,
  ...
}: {
  options.custom = with lib; {
    gnome.enable = mkEnableOption "Enable GNOME desktop";
  };

  config = lib.mkIf config.custom.gnome.enable {
    environment.gnome.excludePackages = with pkgs; [
      epiphany
      gnome-music
      geary
      simple-scan
      gnome-maps
      gnome-software
    ];

    environment.systemPackages = with pkgs;
    with gnomeExtensions; [
      ptyxis
      gnome-extension-manager
      gnome-tweaks

      # Extensions
      blur-my-shell
      dash-to-dock
      tray-icons-reloaded
      vitals
      pano
      forge
      media-progress
      lock-keys
      logo-menu
      alphabetical-app-grid
    ];

    services = {
      gnome = {
        gnome-online-accounts.enable = true;
        sushi.enable = true;
      };

      xserver = {
        desktopManager.gnome = {
          enable = true;
        };

        displayManager.gdm = {
          enable = true;
        };
      };
    };
  };
}
