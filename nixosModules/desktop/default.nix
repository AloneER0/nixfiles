{
  imports = [
    ./plasma6.nix
    ./fonts.nix
    ./gnome.nix
    ./cinnamon.nix
  ];
}
