{
  config,
  lib,
  pkgs,
  ...
}: {
  # Configure Plasma
  options.custom = with lib; {
    cinnamon.enable = mkEnableOption "Cinnamon Desktop";
  };

  config = lib.mkIf config.custom.cinnamon.enable {
    services.xserver.desktopManager = {
      cinnamon = {
        enable = true;
      };
    };

    # Enable SDDM
    services.xserver.displayManager = {
      lightdm = {
        enable = true;
        greeters.slick.enable = true;
      };
    };
  };
}
