{
  config,
  lib,
  pkgs,
  ...
}: {
  # Install all fonts
  options.custom = with lib; {
    allfonts.enable = mkEnableOption "Install all fonts";
  };

  config = lib.mkIf config.custom.allfonts.enable {
    fonts = {
      fontconfig.enable = true;
      fontDir.enable = true;
      packages = with pkgs; [
        inter
        nerd-fonts.jetbrains-mono
        nerd-fonts.iosevka
        nerd-fonts.iosevka-term
        noto-fonts
        noto-fonts-extra
        noto-fonts-cjk-sans
        noto-fonts-cjk-serif
        noto-fonts-emoji
        open-sans
        ubuntu_font_family
        roboto
        roboto-mono
        jetbrains-mono
      ];
    };
  };
}
