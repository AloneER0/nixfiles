{
  lib,
  config,
  ...
}: {
  options.themes = with lib; {
    everblush.enable = mkEnableOption "Enable the Everblush theme";
  };

  config = lib.mkIf config.themes.everblush.enable {
    stylix = {
      base16Scheme = {
        base00 = "#141b1e"; # BASE......."Background"
        base01 = "#0d1214"; # MANTLE....."Background 2"
        base02 = "#232a2d"; # SURFACE0..."Bright Black"
        base03 = "#b3b9b8"; # SURFACE1..."Custom Grey"
        base04 = "#6b7078"; # SURFACE2..."Foreground 5"
        base05 = "#dadada"; # TEXT......."Foreground"
        base06 = "#dadada"; # PRIMARY...."Bright Green"
        base07 = "#dadada"; # SECONDARY.."Green"
        base08 = "#e57474"; # ERR....."Red"
        base09 = "#d58700"; # URG....."Magenta"
        base0A = "#e5c76b"; # WARN...."Yellow"
        base0B = "#8ccf7e"; # TERM...."Green"
        base0C = "#6cbfbf"; # "Cyan"
        base0D = "#67b0e8"; # CURSOR....."Green"
        base0E = "#c47fd5"; # "Magenta"
        base0F = "#a66964"; # "Magenta"
        base12 = "#e57474";
        base13 = "#ffa200";
        base14 = "#8ccf7e";
        base15 = "#6cbfbf";
        base16 = "#67b0e8";
        base17 = "#c47fd5";
      };
    };
  };
}
