{
  lib,
  config,
  ...
}: {
  options.themes = with lib; {
    dark-decay.enable = mkEnableOption "Enable the Dark-Decay theme";
  };

  config = lib.mkIf config.themes.dark-decay.enable {
    stylix = {
      base16Scheme = {
        base00 = "#101419";
        base01 = "#0d1014";
        base02 = "#0f1317";
        base03 = "#484b51";
        base04 = "#6b7078";
        base05 = "#b6beca";
        base06 = "#E3E6EB";
        base07 = "#E3E6EB";
        base08 = "#e05f65";
        base09 = "#d58700";
        base0A = "#f1cf8a";
        base0B = "#90ceaa";
        base0C = "#74bee9";
        base0D = "#70a5eb";
        base0E = "#c68aee";
        base0F = "#dee1e6";
        base12 = "#e5646a";
        base13 = "#ffa200";
        base14 = "#94F7C5";
        base15 = "#79c3ee";
        base16 = "#75aaf0";
        base17 = "#cb8ff3";
      };
    };
  };
}
