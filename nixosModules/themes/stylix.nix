{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./themes
  ];

  options.custom = with lib; {
    themeing.enable = mkEnableOption "Enable Themeing with Stylix";
  };

  config = lib.mkIf config.custom.themeing.enable {
    themes.dark-decay.enable = true;

    stylix = {
      enable = true;

      image = "${../../homeModules/wallpapers/wallpaperflare.com_wallpaper.jpg}";

      cursor = {
        name = "Mocu-White-Right";
        package = pkgs.callPackage ../packages/mocu-xcursor {};
        size = 24;
      };

      targets = {
        plymouth.enable = false;
      };

      fonts = {
        monospace = {
          name = "JetBrains Mono";
          package = pkgs.jetbrains-mono;
        };

        sansSerif = {
          name = "JetBrains Mono";
          package = pkgs.jetbrains-mono;
        };

        sizes = {
          applications = 10;
          terminal = 15;
          desktop = 10;
          popups = 10;
        };
      };
    };
  };
}
