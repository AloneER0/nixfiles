{
  config,
  lib,
  pkgs,
  ...
}: {
  # Enable all gaming stuff

  options.custom = with lib; {
    gaming.enable = mkEnableOption "Enable Gaming stuff";
  };

  config = lib.mkIf config.custom.gaming.enable {
    programs.steam = {
      enable = true;
      remotePlay.openFirewall = true;
      dedicatedServer.openFirewall = true;
      #   (steam.override { extraPkgs = pkgs: [ at-spi2-atk openssl_1_0_2 ]; nativeOnly = false; }).run
    };

    environment.systemPackages = with pkgs; [
      heroic
      bottles
      prismlauncher
      protonup-qt
      osu-lazer-bin
      mangohud
    ];

    programs.gamemode = {
      enable = true;
    };
  };
}
