<table align="center"><tr><td align="center" width="9999">
<img src="./.assets/rainbow-nix.png" align="center" width="150" alt="Project icon">

# Nixfiles

A place for all my Nix configurations!
</td></tr></table>

# Info

Welcome to **NixFiles**!, This is where I store all my NixOS configuration, feel free to take stuff if you want.

| ℹ️ Information                           | 
|------------------------------------------|
| If you see that there is an error somewhere, create a issue and, I will respond as fast as, I can.         |

## Installation

| ⚠️ Warning                              | 
|------------------------------------------|
| My config is pretty much made for my laptop only, so some stuff may not work. Proceed with caution     |

To install it see the next steps:

- Boot into the installer environment
- Format and mount your disks inside /mnt
- execute this

```sh
# go into a root shell
sudo -i

# create this folder if necessary
mkdir -p /mnt/etc/

# clone the repo
git clone https://gitlab.com/alxhr0/nixfiles.git /mnt/etc/nixos --recurse-submodules

# generate the config and take some files
nixos-generate-config --root /mnt
rm /mnt/etc/nixos/configuration.nix
mv /mnt/etc/nixos/hardware-configuration.nix /mnt/etc/nixos/hosts/aether

# make sure you're in this path
cd /mnt/etc/nixos

# Update flake.lock
nix flake update

# to install my acer config
nixos-install --flake '.#aether'
