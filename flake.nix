{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-f2k.url = "github:moni-dz/nixpkgs-f2k";
    nix-colors.url = "github:misterio77/nix-colors";
    #nur.url = "github:nix-community/NUR";
    stylix.url = "github:danth/stylix";
    nixos-boot.url = "github:Melkor333/nixos-boot";

    nixvim = {
      url = "github:nix-community/nixvim";
      # If using a stable channel you can use `url = "github:nix-community/nixvim/nixos-<version>"`
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
    #chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    home-manager,
    #nix-colors,
    #chaotic,
    stylix,
    nixos-boot,
    nix-vscode-extensions,
    nixvim,
    ...
  }: {
    nixosConfigurations = {
      aether = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit self inputs;
        };
        modules = [
          ./hosts/aether
          {
            #nixpkgs.overlays = import ./overlays inputs;
          }
          home-manager.nixosModules.home-manager
          stylix.nixosModules.stylix
          nixos-boot.nixosModules.default
          #chaotic.nixosModules.default
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = {inherit inputs;};
              users.alxhr = import ./homeModules/home.nix;
            };
          }
        ];
      };
    };
    formatter."x86_64-linux" = nixpkgs.legacyPackages."x86_64-linux".alejandra;
  };
}
