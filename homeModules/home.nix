{pkgs, ...}: {
  imports = [
    ./programs
    ./desktop
  ];

  custom = {
    zshshell.enable = true;
    nushell-configured.enable = true; # Enable NuShell (configured)
  };

  programs = {
    bash.enable = true;
    alacritty.enable = true;
  };

  home = {
    username = "alxhr";
    homeDirectory = "/home/alxhr";
    stateVersion = "24.11";

    # Shell aliases
    shellAliases = {
      ls = "eza";
      rebuildnix = "nh os switch";
      dup = "cd /etc/nixos ; nix flake update ; nh os switch";
      ff = "clear ; fastfetch";
      hy = "clear ; hyfetch";
      commit = "git commit -m";
      gadd = "git add . -v";
      gpush = "git push";
      gstash = "git stash";
      gstatus = "git status";
      grep = "rg";
      run = "nix run";
      nsh = "nix shell";
      dbox = "distrobox";
      clr = "clear";
      find = "fd";
      cat = "bat";
      lg = "lazygit";
      mkdir = "mkdir -pv";
      cp = "cp -v";
      rm = "rm -v";
      mv = "mv -v";
      killall = "pkill";
    };
  };
}
