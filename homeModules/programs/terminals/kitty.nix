{
  config,
  lib,
  ...
}: {
  # Kitty
  programs.kitty = {
    enable = true;
    font.name = lib.mkForce "IosevkaTerm Nerd Font";
    font.size = lib.mkForce 15;

    settings = {
      enable_audio_bell = false;
      window_padding_width = 17;
      confirm_os_window_close = -1;
      cursor_shape = "Beam";
    };
  };
}
