{
  imports = [
    ./shells
    ./editors
    ./utilities
    ./terminals
  ];
}
