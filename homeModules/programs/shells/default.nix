{
  imports = [
    ./zsh.nix
    ./starship.nix
    ./fish.nix
    ./direnv.nix
    ./nushell.nix
  ];
}
