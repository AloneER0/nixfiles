{...}: {
  # Enable Direnv
  programs.direnv = {
    enable = true;
  };
}
