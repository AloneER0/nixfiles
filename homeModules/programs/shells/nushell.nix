{
  config,
  lib,
  ...
}: {
  # Enable NuSHell

  options.custom = with lib; {
    nushell-configured.enable = mkEnableOption "Enable NuShell (configured)";
  };

  config = lib.mkIf config.custom.nushell-configured.enable {
    programs.nushell = {
      enable = true;

      extraEnv = ''
        $env.config = {
           show_banner: false,

           hooks: {
               pre_prompt: [{ ||
               if (which direnv | is-empty) {
                 return
               }

               direnv export json | from json | default {} | load-env
               if 'ENV_CONVERSIONS' in $env and 'PATH' in $env.ENV_CONVERSIONS {
                 $env.PATH = do $env.ENV_CONVERSIONS.PATH.from_string $env.PATH
               }
             }]
           }
         }
      '';

      shellAliases = {
        ls = "eza";
        rebuildnix = "nh os switch";
        #dup = "cd /etc/nixos ; nix flake update ; nh os switch";
        #ff = "clear ; fastfetch";
        #hy = "clear ; hyfetch";
        commit = "git commit -m";
        gadd = "git add . -v";
        gpush = "git push";
        gstash = "git stash";
        gstatus = "git status";
        grep = "rg";
        run = "nix run";
        nsh = "nix shell";
        dbox = "distrobox";
        clr = "clear";
        find = "fd";
        cat = "bat";
        lg = "lazygit";
        mkdir = "mkdir -v";
        cp = "cp -v";
        rm = "rm -v";
        mv = "mv -v";
        killall = "pkill";
      };

      extraConfig = ''
        def "dup" [] {
          cd /etc/nixos ; nix flake update ; nh os switch
        }

        def "ff" [] {
          clear ; fastfetch
        }

      '';
    };
  };
}
