{
  config,
  lib,
  ...
}: {
  # Configure ZSH
  options.custom = with lib; {
    zshshell.enable = mkEnableOption "Enable configure ZSH";
  };

  config = lib.mkIf config.custom.zshshell.enable {
    programs.zsh = {
      enable = true;
      syntaxHighlighting.enable = true;
      autosuggestion.enable = true;
      history.size = 10000;
      initExtra = ''
        bindkey '^[\[1;5C' forward-word
        bindkey '^[\[1;5D' backward-word
      '';
    };
  };
}
