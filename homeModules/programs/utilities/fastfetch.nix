{
  config,
  pkgs,
  ...
}: {
  # Configure Fastfetch
  programs.fastfetch = {
    enable = true;

    settings = {
      logo = {
        source = "nixos_small";
        color = {
          "1" = "36";
          "2" = "34";
        };
      };

      modules = [
        "title"
        "separator"
        "os"
        "host"
        "kernel"
        "uptime"
        "packages"
        "shell"
        "de"
        "wm"
        "terminal"
        "cpu"
        "gpu"
        "memory"
        {
          type = "command";
          key = "System Age";
          text = "hourglass";
        }
        {
          type = "command";
          key = "NixOS Generation";
          text = "nix-gen --notext";
        }
        "break"
        "colors"
      ];
    };
  };
}
