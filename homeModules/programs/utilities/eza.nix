{
  config,
  pkgs,
  ...
}: {
  # Enable EZA
  programs = {
    eza = {
      enable = true;
      icons = "always";
      git = true;
    };
  };
}
