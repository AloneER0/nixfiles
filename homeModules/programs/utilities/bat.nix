{config, ...}: {
  # Enable Bat
  programs.bat = {
    enable = true;
    config = {
      style = "plain";
    };
  };
}
