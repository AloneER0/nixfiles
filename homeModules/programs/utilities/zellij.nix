{...}: {
  # Enable zellij
  programs.zellij = {
    enable = true;
    settings = {
      theme = "dark-decay";
      themes = {
        dark-decay = {
          fg = "#b6beca";
          bg = "#101419";
          black = "#1c252c";
          red = "#e05f65";
          green = "#78dba9";
          yellow = "#f1cf8a";
          blue = "#70a5eb";
          magenta = "#c68aee";
          cyan = "#74bee9";
          white = "#dee1e6";
          orange = "#f5a442";
        };
      };
    };
  };
}
