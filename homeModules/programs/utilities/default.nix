{
  imports = [
    ./eza.nix
    ./fastfetch.nix
    ./zoxide.nix
    ./bat.nix
    ./zellij.nix
    ./tmate.nix
    ./direnv.nix
  ];
}
