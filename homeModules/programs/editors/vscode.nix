{
  pkgs,
  inputs,
  lib,
  ...
}: {
  # Enable VSCode
  programs.vscode = {
    enable = true;
    extensions = with inputs.nix-vscode-extensions.extensions.${pkgs.system}.vscode-marketplace; [
      ziglang.vscode-zig
      yzhang.markdown-all-in-one
      golang.go
      ms-python.python
      rust-lang.rust-analyzer
      jnoortheen.nix-ide
      leonardssh.vscord
      #ms-vscode.cpptools
      kamikillerto.vscode-colorize
      nimlang.nimlang
      usernamehw.errorlens
      twxs.cmake
      github.vscode-pull-request-github
      kokakiwi.vscode-just
      ms-vscode.makefile-tools
      arrterian.nix-env-selector
      editorconfig.editorconfig
      mkhl.direnv

      # Themes
      enkia.tokyo-night
      dracula-theme.theme-dracula
      decaycs.decay
      be5invis.vscode-icontheme-nomo-dark
      pkief.material-icon-theme
      akamud.vscode-theme-onedark
      catppuccin.catppuccin-vsc
    ];

    userSettings = {
      "nix.serverPath" = "${pkgs.nixd}/bin/nixd";
      "nix.enableLanguageServer" = true;

      "nix.formatterPath" = "${pkgs.alejandra}/bin/alejandra";

      "editor.fontFamily" = lib.mkForce "Iosevka Nerd Font, monospace";
      "editor.fontSize" = lib.mkForce 17;
      "editor.cursorSmoothCaretAnimation" = "on";
      "editor.fontLigatures" = true;
      "editor.minimap.enabled" = false;

      "colorize.languages" = [
        "css"
        "nix"
        "rust"
        "python"
        "cpp"
        "c"
        "zig"
        "nim"
        "php"
        "javascript"
        "typescript"
        "html"
        "sass"
        "scss"
        "less"
        "postcss"
        "sss"
        "stylus"
        "xml"
        "svg"
        "txt"
      ];

      #"workbench.colorTheme" = "Dark Decay";
      "workbench.iconTheme" = "material-icon-theme";

      "window.menuBarVisibility" = "toggle";
    };
  };
}
