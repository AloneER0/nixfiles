{
  lib,
  config,
  pkgs,
  ...
}: {
  options.abyssmacs = with lib; {
    emacs-treemacs.enable = mkEnableOption "Enable Treemacs";
  };

  config = lib.mkIf config.abyssmacs.emacs-treemacs.enable {
    home.packages = with pkgs; [python3];

    programs.emacs = {
      extraPackages = epkgs: [epkgs.treemacs epkgs.treemacs-evil epkgs.treemacs-nerd-icons];

      extraConfig = ''
        (use-package treemacs
          :ensure t
          :defer t
          :init
            (with-eval-after-load 'winum
            (define-key winum-keymap (kbd "M-d") #'treemacs-select-window))

          :config

          (add-hook 'treemacs-mode-hook (lambda() (display-line-numbers-mode -1)))


          (treemacs-follow-mode t)
        )

        (use-package treemacs-evil
          :after (treemacs evil)
          :ensure t)

        (use-package treemacs-nerd-icons
          :after (treemacs nerd-icons)
          :ensure t)
      '';
    };
  };
}
