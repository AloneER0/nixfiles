{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    emacsdashboard.enable = mkEnableOption "Enable Emacs Dashboard";
  };

  config = lib.mkIf config.abyssmacs.emacsdashboard.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.dashboard];

      extraConfig = ''
        ;; Enable Dashboard
        (use-package dashboard
          :ensure t
          :config
          (setq dashboard-vertically-center-content t)
          (setq dashboard-center-content t)
          (setq dashboard-startup-banner 'logo)
          (dashboard-setup-startup-hook))
      '';
    };
  };
}
