{...}: {
  programs.emacs = {
    extraConfig = ''
      (add-hook 'dired-mode-hook (lambda () (display-line-numbers-mode -1)))
    '';
  };
}
