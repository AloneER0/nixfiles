{...}: {
  programs.emacs = {
    extraConfig = ''
      (menu-bar-mode -1)
      (toggle-scroll-bar -1)
      (global-display-line-numbers-mode 1)
      (tool-bar-mode -1)
      (electric-pair-mode t)
    '';
  };
}
