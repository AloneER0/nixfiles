{
  imports = [
    ./dashboard.nix
    ./window.nix
    ./fonts.nix
    ./nerd-icons.nix
    ./modeline.nix
    ./treemacs.nix
    ./dired.nix
  ];
}
