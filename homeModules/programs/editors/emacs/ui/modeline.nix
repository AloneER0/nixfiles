{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    doom-modeline.enable = mkEnableOption "Enable DOOM Modeline";
  };

  config = lib.mkIf config.abyssmacs.doom-modeline.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.doom-modeline];

      extraConfig = ''
        (use-package doom-modeline
          :ensure t
          :init (doom-modeline-mode 1))
      '';
    };
  };
}
