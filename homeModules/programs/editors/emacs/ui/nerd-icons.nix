{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    emacs-nerdicons.enable = mkEnableOption "Enable Emacs Nerd Icons";
  };

  config = lib.mkIf config.abyssmacs.emacs-nerdicons.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.nerd-icons];

      extraConfig = ''
        ;; Enable Nerd Icons
        (use-package nerd-icons
          :custom
          (nerd-icons-font-family "Iosevka Nerd Font")
        )
      '';
    };
  };
}
