{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    lang-modes.enable = mkEnableOption "Enable Emacs Lang Modes";
  };

  config = lib.mkIf config.abyssmacs.lang-modes.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.nix-mode epkgs.rust-mode epkgs.python-mode epkgs.go-mode epkgs.lua-mode epkgs.yaml-mode epkgs.json-mode epkgs.nushell-mode];

      extraConfig = ''
        ;; Setup languages modes

        ;; Nix
        (use-package nix-mode
          :mode "\\.nix\\'"
          :hook (nix-mode . lsp-deferred))

        ;; Rust
        (use-package rust-mode
          :mode "\\.rs\\'"
          :hook (rust-mode . lsp-deferred))

        ;; Python
        (use-package python-mode
          :mode "\\.py\\'"
          :hook (python-mode . lsp-deferred))

        ;; Go
        (use-package go-mode
          :mode "\\.go\\'"
          :hook (go-mode . lsp-deferred))

        ;; Lua
        (use-package lua-mode
          :mode "\\.lua\\'"
          :hook (lua-mode . lsp-deferred))

        ;; YAML
        (use-package yaml-mode
          :mode "\\.yaml\\'"
          :hook (yaml-mode . lsp-deferred))

        ;; JSON
        (use-package json-mode
          :mode "\\.json\\'"
          :hook (json-mode . lsp-deferred))

        ;; NuShell
        (use-package nushell-mode
          :mode "\\.nu\\'"
          :hook (nushell-mode . lsp-deferred))
      '';
    };
  };
}
