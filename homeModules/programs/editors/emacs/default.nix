{pkgs, ...}: {
  imports = [
    ./ui
    ./binds
    ./utils
    ./languages
    ./lsp
  ];

  programs.emacs = {
    enable = true;
    package = pkgs.emacs-gtk;
  };

  abyssmacs = {
    emacsdashboard.enable = true;
    doom-modeline.enable = true;
    evilmode.enable = true;
    emacs-nerdicons.enable = true;
    emacs-treemacs.enable = true;
    emacs-treesitter.enable = true;
    disable-emacs-backupfiles.enable = true;
    lang-modes.enable = true;
    lsp-mode.enable = true;
    company.enable = true;
    lsp-ui.enable = true;
    flycheck.enable = true;
    direnv.enable = true;
    elcord.enable = true;
  };
}
