{
  imports = [
    ./evil.nix
    ./indent.nix
    ./keybinds.nix
  ];
}
