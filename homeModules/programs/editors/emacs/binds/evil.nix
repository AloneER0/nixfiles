{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    evilmode.enable = mkEnableOption "Enable Emacs Evil Mode";
  };

  config = lib.mkIf config.abyssmacs.evilmode.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.evil];

      extraConfig = ''
        ;; Enable Evil Mode
        (use-package evil
          :init
          (evil-mode 1)
        )
      '';
    };
  };
}
