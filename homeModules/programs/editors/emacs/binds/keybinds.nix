{...}: {
  programs.emacs = {
    extraPackages = epkgs: [epkgs.general];

    extraConfig = ''
      ;; Keybinds
      (use-package general
        :config
          (general-define-key
            "M-d" 'treemacs
            "M-S-C" 'kill-buffer
            "C-S-C" 'kill-ring-save
            "C-S-V" 'yank
          )
      )

    '';
  };
}
