{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    company.enable = mkEnableOption "Enable Emacs Company";
  };

  config = lib.mkIf config.abyssmacs.company.enable {
    programs.emacs = {
      extraPackages = epkgs: [
        epkgs.company
        epkgs.company-box
      ];

      extraConfig = ''
        ;; Enable Company
        (use-package company
          :ensure t
          :config
            (add-hook 'after-init-hook 'global-company-mode))


        ;; Enable Company-box
        (use-package company-box
          :defer t
          :config
           (setq-hook! 'prog-mode-hook
             company-box-frame-top-margin 10)
           (setq-hook! 'text-mode-hook
             company-box-frame-top-margin 10))
      '';
    };
  };
}
