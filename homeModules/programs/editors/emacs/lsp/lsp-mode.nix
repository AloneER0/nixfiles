{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    lsp-mode.enable = mkEnableOption "Enable Emacs LSP Mode";
  };

  config = lib.mkIf config.abyssmacs.lsp-mode.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.lsp-mode];

      extraConfig = ''
        ;; Enable LSP Mode
        (use-package lsp-mode
          :init
          (setq lsp-keymap-prefix "C-c l")
          :commands lsp)
      '';
    };
  };
}
