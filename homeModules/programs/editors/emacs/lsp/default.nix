{
  imports = [
    ./lsp-mode.nix
    ./lsp-ui.nix
    ./flycheck.nix
    ./company.nix
  ];
}
