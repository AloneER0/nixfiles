{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    flycheck.enable = mkEnableOption "Enable Emacs Flycheck";
  };

  config = lib.mkIf config.abyssmacs.flycheck.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.flycheck];

      extraConfig = ''
        ;; Enable Flycheck
        (use-package flycheck
          :ensure t
          :config
          (add-hook 'after-init-hook #'global-flycheck-mode))
      '';
    };
  };
}
