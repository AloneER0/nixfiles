{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    lsp-ui.enable = mkEnableOption "Enable Emacs LSP Ui";
  };

  config = lib.mkIf config.abyssmacs.lsp-ui.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.lsp-ui];

      extraConfig = ''
        ;; Enable LSP Ui
        (use-package lsp-ui)
      '';
    };
  };
}
