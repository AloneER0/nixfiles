{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    elcord.enable = mkEnableOption "Enable Elcord";
  };

  config = lib.mkIf config.abyssmacs.elcord.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.elcord];

      extraConfig = ''
        ;; Elcord
        (use-package elcord
          :init
          (elcord-mode))
      '';
    };
  };
}
