{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    direnv.enable = mkEnableOption "Enable Direnv support";
  };

  config = lib.mkIf config.abyssmacs.direnv.enable {
    programs.emacs = {
      extraPackages = epkgs: [epkgs.direnv];

      extraConfig = ''
        ;; Direnv
        (use-package direnv
          :config
          (direnv-mode))
      '';
    };
  };
}
