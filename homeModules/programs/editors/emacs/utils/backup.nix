{
  lib,
  config,
  ...
}: {
  options.abyssmacs = with lib; {
    disable-emacs-backupfiles.enable = mkEnableOption "Disable Emacs backup files";
  };

  config = lib.mkIf config.abyssmacs.disable-emacs-backupfiles.enable {
    programs.emacs = {
      extraConfig = ''
        ;; Disable Emacs backup files
        (setq make-backup-files nil)
      '';
    };
  };
}
