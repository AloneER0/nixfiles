{
  lib,
  config,
  ...
}: {
  # TODO: Switch to peek.nvim
  options.abyssnvim = {
    markdown-preview.enable = lib.mkEnableOption "Enable markdown-preview module";
  };
  config = lib.mkIf config.abyssnvim.markdown-preview.enable {
    programs.nixvim.plugins.markdown-preview = {
      enable = true;
      settings = {
        browser = "firefox";
        theme = "dark";
      };
    };

    programs.nixvim.keymaps = [
      {
        mode = "n";
        key = "<leader>cp";
        action = "<cmd>MarkdownPreview<cr>";
        options = {
          desc = "Markdown Preview";
        };
      }
    ];
  };
}
