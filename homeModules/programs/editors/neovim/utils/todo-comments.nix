{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    todo-comments.enable = lib.mkEnableOption "Enable todo-comments module";
  };
  config = lib.mkIf config.abyssnvim.todo-comments.enable {
    programs.nixvim.plugins.todo-comments = {
      enable = true;
    };
  };
}
