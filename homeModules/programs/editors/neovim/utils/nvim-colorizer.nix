{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    nvim-colorizer.enable = lib.mkEnableOption "Enable nvim-colorizer module";
  };
  config = lib.mkIf config.abyssnvim.nvim-colorizer.enable {
    programs.nixvim.plugins.colorizer = {
      enable = true;
    };
  };
}
