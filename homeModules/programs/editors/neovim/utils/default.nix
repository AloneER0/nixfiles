{
  imports = [
    ./nvterm.nix
    ./neocord.nix
    ./todo-comments.nix
    ./nvim-colorizer.nix
    ./nvim-autopairs.nix
    ./markdown-preview.nix
    ./ultimate-autopair.nix
    ./telescope-nvim.nix
  ];
}
