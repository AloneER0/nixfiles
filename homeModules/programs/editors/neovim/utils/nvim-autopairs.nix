{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    nvim-autopairs.enable = lib.mkEnableOption "Enable nvim-autopairs module";
  };
  config = lib.mkIf config.abyssnvim.nvim-autopairs.enable {
    programs.nixvim.plugins.nvim-autopairs = {
      enable = true;
    };
  };
}
