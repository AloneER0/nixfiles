{
  imports = [
    ./conform.nix
    ./fidget.nix
    ./lsp-nvim.nix
    ./lspsaga.nix
    ./trouble.nix
  ];
}
