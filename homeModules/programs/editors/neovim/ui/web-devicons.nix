{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    web-devicons.enable = lib.mkEnableOption "Enable web-devicons module";
  };
  config = lib.mkIf config.abyssnvim.web-devicons.enable {
    programs.nixvim.plugins.web-devicons = {
      enable = true;
    };
  };
}
