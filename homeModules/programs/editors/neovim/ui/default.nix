{
  imports = [
    ./alpha.nix
    ./web-devicons.nix
    ./dressing-nvim.nix
    ./nvim-lualine.nix
  ];
}
