{
  config,
  lib,
  ...
}: {
  # Enable Neovim Lualine
  options.abyssnvim = {
    nvim-lualine.enable = lib.mkEnableOption "Enable Nvim Lualine";
  };

  config = lib.mkIf config.abyssnvim.nvim-lualine.enable {
    programs.nixvim.plugins.lualine = {
      enable = true;
    };
  };
}
