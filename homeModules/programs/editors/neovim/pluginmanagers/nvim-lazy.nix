{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    lazy-nvim.enable = lib.mkEnableOption "Enable lazy-nvim module";
  };
  config = lib.mkIf config.abyssnvim.lazy-nvim.enable {
    programs.nixvim.plugins.lazy = {
      enable = true;
    };
  };
}
