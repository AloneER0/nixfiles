{
  lib,
  config,
  ...
}: {
  options.abyssnvim = {
    lspkind.enable = lib.mkEnableOption "Enable lspkind module";
  };
  config = lib.mkIf config.abyssnvim.lspkind.enable {
    programs.nixvim.plugins.lspkind = {
      enable = true;
      symbolMap = {
        Copilot = "";
      };
      extraOptions = {
        maxwidth = 50;
        ellipsis_char = "...";
      };
    };
  };
}
