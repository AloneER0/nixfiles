{
  config,
  lib,
  ...
}: {
  options.abyssnvim = {
    enable = lib.mkEnableOption "Enable AbyssNvim";
  };

  config = lib.mkIf config.abyssnvim.enable {
    programs.nixvim = {
      enable = true;

      extraConfigLua = ''
        vim.opt.expandtab = true
        vim.opt.tabstop = 4
        vim.opt.shiftwidth = 4
        vim.opt.softtabstop = 4

        vim.opt.number = true
      '';
    };
  };
}
