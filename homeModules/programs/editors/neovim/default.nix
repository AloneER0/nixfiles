{inputs, ...}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    ./general
    ./ui
    ./lsp
    ./completions
    ./utils
    ./pluginmanagers
  ];

  abyssnvim = {
    enable = true;

    # Plugins
    alpha.enable = true;
    web-devicons.enable = true;
    lspsaga.enable = true;
    fidget.enable = true;
    trouble.enable = true;
    conform.enable = true;
    lsp-nvim.enable = true;
    lspkind.enable = true;
    cmp.enable = true;
    neocord.enable = true;
    ultimate-autopair.enable = true;
    markdown-preview.enable = true;
    nvim-colorizer.enable = true;
    nvterm.enable = true;
    todo-comments.enable = true;
    nvim-autopairs.enable = true;
    dressing-nvim.enable = true;
    lazy-nvim.enable = true;
    nvim-lualine.enable = true;
    telescope-nvim.enable = true;
  };
}
