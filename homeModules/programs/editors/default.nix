{
  imports = [
    ./vscode.nix
    ./neovim
    ./emacs
  ];
}
