{
  inputs,
  pkgs,
  ...
}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
  ];

  # Enable Neovim
  programs.nixvim = {
    enable = true;

    colorschemes.tokyonight.enable = true;

    plugins = {
      treesitter = {
        enable = true;
        settings = {
          highlight.enable = true;
        };
      };

      twilight.enable = true;
      telescope.enable = true;
      web-devicons.enable = true;

      lsp = {
        enable = true;
        servers = {
          clangd.enable = true;
          nixd.enable = true;
          ts_ls.enable = true;
          lua_ls.enable = true;
          pyright.enable = true;
          rust_analyzer = {
            enable = true;
            installCargo = true;
            installRustc = true;
            settings = {
              checkOnSave = true;
              check = {
                command = "${pkgs.clippy}/bin/clippy";
              };
            };
          };
        };
      };

      indent-blankline = {
        enable = true;
        settings = {
          indent = {
            char = "│"; # "│" or "▎"
          };
          scope = {
            enabled = true;
            show_start = true;
          };
          exclude = {
            buftypes = [
              "terminal"
              "nofile"
            ];
            filetypes = [
              "help"
              "alpha"
              "dashboard"
              "neo-tree"
              "Trouble"
              "trouble"
              "lazy"
              "mason"
              "notify"
              "toggleterm"
              "lazyterm"
              "nvterm"
            ];
          };
        };
      };

      lsp-format.enable = true;

      alpha = {
        enable = true;
        theme = null;
        layout = let
          padding = val: {
            type = "padding";
            inherit val;
          };
        in [
          (padding 4)
          {
            opts = {
              hl = "AlphaHeader";
              position = "center";
            };
            type = "text";
            val = [
              "   ███▄    █ ▓█████ ██▒   █▓▓█████   "
              "   ██ ▀█   █ ▓█   ▀▓██░   █▒▓█   ▀   "
              "  ▓██  ▀█ ██▒▒███   ▓██  █▒░▒███     "
              "  ▓██▒  ▐▌██▒▒▓█  ▄  ▒██ █░░▒▓█  ▄   "
              "  ▒██░   ▓██░░▒████▒  ▒▀█░  ░▒████▒  "
              "  ░ ▒░   ▒ ▒ ░░ ▒░ ░  ░ ▐░  ░░ ▒░ ░  "
              "  ░ ░░   ░ ▒░ ░ ░  ░  ░ ░░   ░ ░  ░  "
              "     ░   ░ ░    ░       ░░     ░     "
              "           ░    ░  ░     ░     ░  ░  "
              "                        ░            "
              "                                     "
              "      git@github.com:redyf/neve      "
            ];
          }
          (padding 2)
          {
            type = "button";
            val = "  Find File";
            on_press = {
              __raw = "function() require('telescope.builtin').find_files() end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "f"
                ":Telescope find_files <CR>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "f";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
          (padding 1)
          {
            type = "button";
            val = "  New File";
            on_press = {
              __raw = "function() vim.cmd[[ene]] end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "n"
                ":ene <BAR> startinsert <CR>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "n";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
          (padding 1)
          {
            type = "button";
            val = "󰈚  Recent Files";
            on_press = {
              __raw = "function() require('telescope.builtin').oldfiles() end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "r"
                ":Telescope oldfiles <CR>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "r";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
          (padding 1)
          {
            type = "button";
            val = "󰈭  Find Word";
            on_press = {
              __raw = "function() require('telescope.builtin').live_grep() end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "g"
                ":Telescope live_grep <CR>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "g";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
          (padding 1)
          {
            type = "button";
            val = "  Restore Session";
            on_press = {
              __raw = "function() require('persistence').load() end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "s"
                ":lua require('persistence').load()<cr>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "s";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
          (padding 1)
          {
            type = "button";
            val = "  Quit Neovim";
            on_press = {
              __raw = "function() vim.cmd[[qa]] end";
            };
            opts = {
              # hl = "comment";
              keymap = [
                "n"
                "q"
                ":qa<CR>"
                {
                  noremap = true;
                  silent = true;
                  nowait = true;
                }
              ];
              shortcut = "q";

              position = "center";
              cursor = 3;
              width = 38;
              align_shortcut = "right";
              hl_shortcut = "Keyword";
            };
          }
        ];
      };
    };
  };
}
