{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    alacritty.enable = mkEnableOption "Enable ML4W Alacritty";
  };

  config = lib.mkIf config.ml4w.alacritty.enable {
    # Alacritty
    programs.alacritty = {
      enable = true;
      settings = lib.mkForce {
        font = {
          normal = {
            family = "FiraCode";
            style = "Regular";
          };

          size = 12;
        };

        window = {
          opacity = 0.8;
        };

        selection.save_to_clipboard = true;
      };
    };
  };
}
