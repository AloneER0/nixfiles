{
  imports = [
    ./hyprland
    ./alacritty
    ./dunst
    ./rofi
    ./waybar
    ./wlogout
  ];

  ml4w = {
    # Hyprland
    hyprland = {
      enable = true;
      animations.enable = true;
      autostart.enable = true;
      binds.enable = true;
      decoration.enable = true;
      environment.enable = true;
      gestures.enable = true;
      input.enable = true;
      layouts.enable = true;
      misc.enable = true;
      monitor.enable = true;
      hyprpaper.enable = true;
      hyprlock.enable = true;
    };

    # Apps
    alacritty.enable = true;
    dunst.enable = true;
    rofi.enable = true;
    waybar.enable = true;
    wlogout.enable = true;
  };
}
