{
  config,
  lib,
  pkgs,
  ...
}: {
  options.ml4w = with lib; {
    rofi.enable = mkEnableOption "Enable ML4W Rofi";
  };

  config = lib.mkIf config.ml4w.rofi.enable {
    # Rofi
    programs.rofi = {
      enable = true;
      font = lib.mkForce "Fira Sans 11";
      package = pkgs.rofi-wayland;

      extraConfig = lib.mkForce {
        modi = "drun";
        show-icons = true;
        display-drun = "APPS";
        display-run = "RUN";
        display-filebrowser = "FILES";
        display-window = "WINDOW";
        hover-select = false;
        scroll-method = 1;
        me-select-entry = "";
        me-accept-entry = "MousePrimary";
        drun-display-format = "{name}";
        window-format = "{w} · {c} · {t}";
      };

      theme = let
        inherit (config.lib.formats.rasi) mkLiteral;
      in
        lib.mkForce {
          "*" = {
            background = mkLiteral "rgba(0,0,1,0.5)";
            foreground = mkLiteral "#FFFFFF";
            color0 = mkLiteral "#0A121B";
            color1 = mkLiteral "#056F93";
            color2 = mkLiteral "#039AA5";
            color3 = mkLiteral "#02C2BC";
            color4 = mkLiteral "#02A4D2";
            color5 = mkLiteral "#01AFEE";
            color6 = mkLiteral "#01D3D2";
            color7 = mkLiteral "#77e8e1";
            color8 = mkLiteral "#53a29d";
            color9 = mkLiteral "#056F93";
            color10 = mkLiteral "#039AA5";
            color11 = mkLiteral "#02C2BC";
            color12 = mkLiteral "#02A4D2";
            color13 = mkLiteral "#01AFEE";
            color14 = mkLiteral "#01D3D2";
            color15 = mkLiteral "#77e8e1";
            border-width = "3px";
          };

          window = {
            width = mkLiteral "600px";
            x-offset = mkLiteral "0px";
            y-offset = mkLiteral "65px";
            spacing = mkLiteral "0px";
            padding = mkLiteral "0px";
            margin = mkLiteral "0px";
            color = mkLiteral "#FFFFFF";
            border = mkLiteral "@border-width";
            border-color = mkLiteral "#FFFFFF";
            text-color = "@foreground";
            cursor = mkLiteral "'default'";
            transparency = mkLiteral "'real'";
            location = mkLiteral "north";
            anchor = mkLiteral "north";
            fullscreen = false;
            enabled = true;
            border-radius = mkLiteral "10px";
            background-color = mkLiteral "transparent";
          };

          mainbox = {
            enabled = true;
            orientation = mkLiteral "horizontal";
            spacing = mkLiteral "0px";
            margin = mkLiteral "0px";
            background-color = mkLiteral "@background";
            text-color = "@foreground";
            children = ["listbox"];
          };

          imagebox = {
            padding = mkLiteral "18px";
            background-color = mkLiteral "transparent";
            orientation = mkLiteral "vertical";
            text-color = "@foreground";
            children = [
              "inputbar"
              "dummy"
              "mode-switcher"
            ];
          };

          listbox = {
            spacing = mkLiteral "20px";
            background-color = mkLiteral "transparent";
            text-color = "@foreground";
            orientation = mkLiteral "vertical";
            children = [
              "inputbar"
              "message"
              "listview"
            ];
          };

          dummy = {
            background-color = mkLiteral "transparent";
            text-color = "@foreground";
          };

          inputbar = {
            enabled = true;
            text-color = mkLiteral "@foreground";
            spacing = mkLiteral "10px";
            padding = mkLiteral "15px";
            border-radius = mkLiteral "0px";
            border-color = mkLiteral "@foreground";
            background-color = mkLiteral "@background";
            children = [
              "textbox-prompt-colon"
              "entry"
            ];
          };

          textbox-prompt-colon = {
            enabled = true;
            expand = false;
            padding = mkLiteral "0px 5px 0px 0px";
            str = mkLiteral "' '";
            background-color = mkLiteral "transparent";
            text-color = mkLiteral "@foreground";
          };

          entry = {
            enabled = true;
            background-color = mkLiteral "transparent";
            text-color = mkLiteral "@foreground";
            cursor = mkLiteral "text";
            placeholder = mkLiteral "'Search'";
            placeholder-color = mkLiteral "@foreground";
          };

          mode-switcher = {
            enabled = true;
            spacing = mkLiteral "20px";
            background-color = mkLiteral "transparent";
            text-color = mkLiteral "@foreground";
          };

          button = {
            padding = mkLiteral "10px";
            border-radius = mkLiteral "10px";
            background-color = mkLiteral "@background";
            text-color = mkLiteral "@foreground";
            cursor = mkLiteral "pointer";
            border = mkLiteral "0px";
          };

          button-selected = {
            background-color = mkLiteral "@color11";
            text-color = mkLiteral "@foreground";
          };

          listview = {
            enabled = true;
            columns = 1;
            lines = 8;
            cycle = false;
            dynamic = false;
            scrollbar = false;
            layout = mkLiteral "vertical";
            reverse = false;
            fixed-height = true;
            fixed-columns = true;
            spacing = mkLiteral "0px";
            text-color = "@foreground";
            padding = mkLiteral "10px";
            margin = mkLiteral "0px";
            background-color = mkLiteral "@background";
            border = mkLiteral "0px";
          };

          element = {
            enabled = true;
            padding = mkLiteral "10px";
            margin = mkLiteral "5px";
            cursor = mkLiteral "pointer";
            text-color = "@foreground";
            background-color = mkLiteral "@background";
            border-radius = mkLiteral "10px";
            border = mkLiteral "@border-width";
          };

          element-normal-normal = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-normal-urgent = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-normal-active = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-selected-normal = {
            background-color = mkLiteral "@color11";
            text-color = mkLiteral "@foreground";
          };

          element-selected-urgent = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-selected-active = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-alternate-normal = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-alternate-urgent = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-alternate-active = {
            background-color = mkLiteral "inherit";
            text-color = mkLiteral "@foreground";
          };

          element-icon = {
            background-color = mkLiteral "transparent";
            text-color = mkLiteral "@foreground";
            size = mkLiteral "32px";
            cursor = mkLiteral "inherit";
          };

          element-text = {
            background-color = mkLiteral "transparent";
            text-color = "@foreground";
            cursor = mkLiteral "inherit";
            vertical-align = mkLiteral "0.5";
            horizontal-align = mkLiteral "0.0";
          };

          message = {
            background-color = mkLiteral "transparent";
            border = mkLiteral "0px";
            margin = mkLiteral "20px 0px 0px 0px";
            padding = mkLiteral "0px";
            spacing = mkLiteral "0px";
            text-color = "@foreground";
            border-radius = mkLiteral "10px";
          };

          textbox = {
            padding = mkLiteral "15px";
            margin = mkLiteral "0px";
            border-radius = mkLiteral "0px";
            background-color = mkLiteral "@background";
            text-color = mkLiteral "'#FFFFFF'";
            vertical-align = mkLiteral "0.5";
            horizontal-align = mkLiteral "0.0";
          };

          error-message = {
            padding = mkLiteral "15px";
            border-radius = mkLiteral "20px";
            background-color = mkLiteral "@background";
            text-color = mkLiteral "@foreground";
          };
        };
    };
  };
}
