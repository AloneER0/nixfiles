{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.hyprpaper.enable = mkEnableOption "Enable ML4W Hyprpaper";
  };

  config = lib.mkIf config.ml4w.hyprland.hyprpaper.enable {
    # Hyprpaper
    services.hyprpaper = {
      enable = true;

      settings = {
        splash = false;

        preload = lib.mkForce "${../wallpapers/wallpaper.jpg}";
        wallpaper = lib.mkForce ",${../wallpapers/wallpaper.jpg}";
      };
    };
  };
}
