{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.binds.enable = mkEnableOption "Enable ML4W Hyprland binds";
  };

  config = lib.mkIf config.ml4w.hyprland.binds.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = let
        mod = "SUPER";
        file_manager = "thunar";
        browser = "firefox";
      in {
        bind =
          [
            # Actions
            "${mod}, return, exec, alacritty"
            "${mod}, Q, killactive"
            "${mod}, M, exit"
            "${mod}, E, exec, ${file_manager}"
            "${mod}, T, togglefloating"
            "${mod}, F, fullscreen"
            "${mod}  CTRL, return, exec, rofi -show drun "
            "${mod}, P, pseudo"
            "${mod}, J, togglesplit"
            "${mod}, B, exec, ${browser}"
            "${mod}  SHIFT, B, exec, ~/.config/ml4w/scripts/reload-waybar.sh"
            "${mod}  SHIFT, W, exec, ~/.config/ml4w/scripts/reload-hyprpaper.sh"

            # Move focus with arrow keys
            "${mod}, left, movefocus, l"
            "${mod}, right, movefocus, r"
            "${mod}, top, movefocus, u"
            "${mod}, down, movefocus, d"

            #  Scroll through workspaces with mod + scroll
            "${mod}, mouse_down, workspace, e+1"
            "${mod}, mouse_up, workspace, e-1"
          ]
          ++ (
            # Workspaces
            # binds mod + [SHIFT +] {1..10} to [move to] workspace {1..10}
            builtins.concatLists (
              builtins.genList (
                x: let
                  ws = let
                    c = (x + 1) / 10;
                  in
                    builtins.toString (x + 1 - (c * 10));
                in [
                  "${mod}, ${ws}, workspace, ${toString (x + 1)}"
                  "${mod} SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
                ]
              )
              10
            )
          );

        bindm = [
          "${mod}, mouse:272, movewindow"
          "${mod}, mouse:273, resizewindow"
        ];
      };
    };
  };
}
