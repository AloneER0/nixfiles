{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.layouts.enable = mkEnableOption "Enable ML4W Hyprland layouts";
  };

  config = lib.mkIf config.ml4w.hyprland.layouts.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        dwindle = {
          pseudotile = true;
          preserve_split = true;
        };
      };
    };
  };
}
