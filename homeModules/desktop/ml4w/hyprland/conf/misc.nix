{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.misc.enable = mkEnableOption "Enable ML4W Hyprland misc";
  };

  config = lib.mkIf config.ml4w.hyprland.misc.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        misc = {
          "disable_hyprland_logo" = true;
          "disable_splash_rendering" = true;
        };
      };
    };
  };
}
