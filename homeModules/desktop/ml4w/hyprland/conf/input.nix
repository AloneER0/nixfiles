{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.input.enable = mkEnableOption "Enable ML4W Hyprland input";
  };

  config = lib.mkIf config.ml4w.hyprland.input.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        input = {
          kb_layout = "us";
          follow_mouse = 1;

          touchpad = {
            "natural_scroll" = false;
          };

          sensitivity = 0;
        };
      };
    };
  };
}
