{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.gestures.enable = mkEnableOption "Enable ML4W Hyprland gestures";
  };

  config = lib.mkIf config.ml4w.hyprland.gestures.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        gestures = {
          "workspace_swipe" = true;
        };
      };
    };
  };
}
