{
  imports = [
    ./animations.nix
    ./autostart.nix
    ./binds.nix
    ./decoration.nix
    ./environment.nix
    ./gestures.nix
    ./input.nix
    ./layouts.nix
    ./misc.nix
    ./monitor.nix
  ];
}
