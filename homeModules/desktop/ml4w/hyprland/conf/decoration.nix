{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.decoration.enable = mkEnableOption "Enable ML4W Hyprland decoration";
  };

  config = lib.mkIf config.ml4w.hyprland.decoration.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        decoration = {
          rounding = 10;

          blur = {
            enabled = true;
            size = 3;
            passes = 1;
          };
        };
      };
    };
  };
}
