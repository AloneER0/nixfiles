{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.monitor.enable = mkEnableOption "Enable ML4W Hyprland monitor config";
  };

  config = lib.mkIf config.ml4w.hyprland.monitor.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      settings = {
        monitor = ",preferred,auto,1";
      };
    };
  };
}
