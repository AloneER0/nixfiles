{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.autostart.enable = mkEnableOption "Enable ML4W Hyprland autostart";
  };

  config = lib.mkIf config.ml4w.hyprland.autostart.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      extraConfig = ''
        # Execute your favorite apps at launch
        exec-once = waybar
        exec-once = hyprpaper
        exec-once = dunst

        # Load configuration from ML4W Hyprland Settings App
        exec = ~/.config/ml4w-hyprland-settings/hyprctl.sh
      '';
    };
  };
}
