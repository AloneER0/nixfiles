{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.enable = mkEnableOption "Enable ML4W Hyprland";
  };

  config = lib.mkIf config.ml4w.hyprland.enable {
    # Hyprland
    wayland.windowManager.hyprland = {
      enable = true;
      settings = {
        general = {
          gaps_in = 5;
          gaps_out = 20;
          border_size = 2;
          "col.active_border" = lib.mkForce "rgba(33ccffee) rgba(00ff99ee) 45deg";
          "col.inactive_border" = lib.mkForce "rgba(595959aa)";
          layout = "dwindle";
          resize_on_border = true;
        };
      };
    };
  };
}
