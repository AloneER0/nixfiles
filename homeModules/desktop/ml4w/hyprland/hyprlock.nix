{
  config,
  lib,
  ...
}: {
  options.ml4w = with lib; {
    hyprland.hyprlock.enable = mkEnableOption "Enable ML4W Hyprlock";
  };

  config = lib.mkIf config.ml4w.hyprland.hyprlock.enable {
    # Hyprlock
    programs.hyprlock = {
      enable = true;

      settings = {
        background = lib.mkForce [
          {
            path = "${../wallpapers/wallpaper.jpg}";
          }
        ];

        input-field = lib.mkForce [
          {
            size = "200, 50";
            outline_thickness = 3;
            dots_size = 0.33;
            dots_spacing = 0.15;
            dots_center = true;
            dots_rounding = -1;
            outer_color = "rgb(151515)";
            inner_color = "rgb(FFFFFF)";
            font_color = "rgb)10, 10, 10";
            fade_on_empty = true;
            fade_timeout = 1000;
            placeholder_text = "<i>Input Password...</i>";
            hide_input = false;
            rounding = -1;
            check_color = "rgb(204, 126, 34)";
            fail_color = "rgb(204, 34, 34)";
            faIl_text = "<i>$FAIL <b>($ATTEMPTS)</b></i>";
            faIl_transaction = 300;
            capslock_color = -1;
            numlock_color = -1;
            bothlock_color = -1;
            invert_numlock = true;
            swap_font_color = false;
            position = "0, -20";
            halign = "center";
            valign = "center";
          }
        ];

        label = lib.mkForce [
          {
            text = "cmd[update:1000] echo '$TIME'";
            color = "rgba(200, 200, 200, 1.0)";
            font_size = 55;
            font_family = "Fira Semibold";
            position = "-100, -40";
            halign = "right";
            valign = "bottom";
            shadow_passes = 5;
            shadow_size = 10;
          }
        ];

        label2 = [
          {
            text = "$USER";
            color = "rgba(200, 200, 200, 1.0)";
            font_size = 20;
            font_family = "Fira Semibold";
            position = "-100, 160";
            halign = "right";
            valign = "bottom";
            shadow_passes = 5;
            shadow_size = 10;
          }
        ];
      };
    };
  };
}
