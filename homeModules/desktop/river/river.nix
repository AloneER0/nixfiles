{config, ...}: {
  # Enable River
  programs.river = {
    enable = true;
    settings = {
      border-width = 2;
      spawn = [
        "waybar"
        "xwaylandvideobridge"
      ];

      map = {
        normal = {
          "Super Return" = "alacritty";
          "Super W" = "close";
          "Super Q" = "exit";
        };
      };
    };
  };
}
