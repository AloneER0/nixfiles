{config, ...}: {
  # Change the NixOS logo to the official one, no matter the icon theme
  xdg.configFile."kcm-about-distrorc" = {
    text = ''
      [General]
      LogoPath=${./images/nixos.svg}
    '';
  };
}
